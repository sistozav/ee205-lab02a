# Build a datatype explorer program

HOSTNAME=$(shell hostname --short)
$(info $(HOSTNAME))

CC     = gcc
CFLAGS = -g -Wall -DHOST=\"$(HOSTNAME)\"

TARGET = datatypes

all: $(TARGET)

# For now, do not use Make's % expansion parameters.  Each file must have its own 
# build instructions.  We will use the % expansion later.

datatypes.o: datatypes.c datatypes.h char.h short.h int.h long.h
	$(CC) $(CFLAGS) -c datatypes.c

int.o: int.c
	$(CC) $(CFLAGS) -c int.c

long.o: long.c
	$(CC) $(CFLAGS) -c long.c

char.o: char.c
	$(CC) $(CFLAGS) -c char.c

short.o: short.c
	$(CC) $(CFLAGS) -c short.c

datatypes: datatypes.o char.o short.o int.o long.o
	$(CC) $(CFLAGS) -o $(TARGET) datatypes.o char.o short.o int.o long.o

datatypes.o: datatypes.h

int.o: int.h

char.o: char.h

short.o: short.h

long.o: long.h

clean:
	rm -f *.o $(TARGET)

